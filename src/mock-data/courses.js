export default [
{
	id: "wdc001",
	name: "PHP-Laravel",
	description: "Test Course",
	price: 45000,
	onOffer: true
},
{
	id: "wdc002",
	name: "Python - Django",
	description: "Test Course",
	price: 50000,
	onOffer: true
},
{
	id: "wdc003",
	name: "Java - Springboot",
	description: "Test Course",
	price: 55000,
	onOffer: true
}
]