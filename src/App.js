import React, {Fragment} from 'react'; 

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
// import CourseCard from './components/CourseCard';

import Courses from './pages/Courses'

export default function App(){
	return (
		<Fragment>
		    <AppNavbar />
		    <Home />
		    <Courses/>
		  </Fragment>
		  )
}